
from kivy.app import App

from plyer import notification

"""
    Kivy example by Alexander Taylor:
    https://www.youtube.com/user/kivycrashcourse
"""

class AndroidApp(App):
    def build(self):
        notification.notify('Some title', 'Some message text')

AndroidApp().run()