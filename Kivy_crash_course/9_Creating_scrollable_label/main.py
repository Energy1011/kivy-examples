from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView

from kivy.base import runTouchApp
from kivy.properties import StringProperty
from kivy.base import Builder

"""
	Kivy example by Alexander Taylor:
	https://www.youtube.com/user/kivycrashcourse
"""

Builder.load_string('''
<ScrollableLabel>
	text: str("this is my long long text" * 40)
	Label:
		text: root.text
		font_size: 50
		text_size: self.width, None
		size_hint_y: None
		height: self.texture_size[1]
''')


class ScrollableLabel(ScrollView):
	text = StringProperty('')
	pass

runTouchApp(ScrollableLabel())
