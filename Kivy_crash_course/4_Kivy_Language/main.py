from kivy.app import App
from kivy.uix.scatter import Scatter
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout

"""
	Kivy example by Alexander Taylor:
	https://www.youtube.com/user/kivycrashcourse
"""
class ScatterTextWidget(BoxLayout):
	pass
class TutorialApp(App):
	def build(self):
		return ScatterTextWidget()

if __name__ == "__main__":
	TutorialApp().run()