from kivy.app import App
from kivy.clock import Clock
from kivy.uix.widget import Widget

class MyApp(App):
    def my_callback(self, value):
        print "This is my callback"
        pass

    def build(self):
        # call my_callback every 0.5 seconds
        Clock.schedule_interval(self.my_callback, 0.5)

        # call my_callback in 5 seconds
        Clock.schedule_once(self.my_callback, 5)

        # call my_callback as soon as possible (usually next frame.)
        Clock.schedule_once(self.my_callback)
        return Widget()

if __name__ == '__main__':
    MyApp().run()