#!/bin/env python
"""
Composite List Example
"""
from kivy.app import App
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.dictadapter import DictAdapter
from kivy.uix.listview import ListItemButton, ListItemLabel, ListView, CompositeListItem
from kivy.lang import Builder
from kivy.uix.checkbox import CheckBox

class MyCompositeList(App):

    def build(self):
      args_converter = lambda row_index, rec: \
          {'text': rec['text'],
          'size_hint_y': None,
          'height': 25,
          'cls_dicts': [{'cls': ListItemButton,
                          'kwargs': {'text': rec['text']}},
                        {'cls': ListItemLabel,
                          'kwargs': {'text': "Label-{0}".format(rec['text']),
                                  'is_representing_cls': True}}
                        ]}

      item_strings = ["{0}".format(index) for index in range(100)]

      integers_dict = \
          {str(i): {'text': str(i), 'is_selected': False} for i in range(100)}

      dict_adapter = DictAdapter(sorted_keys=item_strings,
                                 data=integers_dict,
                                 args_converter=args_converter,
                                 selection_mode='single',
                                 allow_empty_selection=False,
                                 cls=CompositeListItem)

      list_view = ListView(adapter=dict_adapter)

      return list_view


if __name__ == '__main__':
    MyCompositeList().run()