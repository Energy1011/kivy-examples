from kivy.core.window import Window
from kivy.app import App
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
import random

"""
Moving labels example
"""

class LabelWord(FloatLayout):

    def __init__(self, **kvargs):
        super(LabelWord, self).__init__(**kvargs)
        self.label = Label(text="A[color=ff3333]Red[/color]BC", font_size=20, markup=True)
        # length = len(l.text) check length
        self.label.pos_hint={'x':.0, 'y':random.uniform(-0.50, .50)} # limit from (-.50 to .50)
        self.add_widget(self.label)


class MyLabels(FloatLayout):
    l_list = []
    selected_label = None

    def __init__(self, **kvargs):
        super(MyLabels, self).__init__(**kvargs)

    def create_label(self):
        l = LabelWord()
        self.add_widget(l)
        self.l_list.append(l)
        self.selected_label = len(self.l_list) - 1

    def update(self, dt):
        # move the label
        for l in self.l_list:
            new_pos = l.label.pos_hint
            if new_pos['x'] - 0.01 <= -0.50:
                new_pos['x'] = .50
            l.label.pos_hint={'x':new_pos['x'] - 0.01, 'y':new_pos['y']}
        pass


class MovingLabelApp(App):
    labels = MyLabels()

    def get_keying(self, input, text):
        print text
        current_label = self.labels.selected_label
        input.text=""
        print self.labels.l_list[current_label].label.text
        # self.labels.my_labels

    def build(self):
        # self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        # self._keyboard.bind(on_key_down=self._on_keyboard_down)
        main_textinput = TextInput(font_size=20,
            size_hint_y=None,
            height=40,
            text='')
        main_textinput.bind(text=self.get_keying)
        self.labels.add_widget(main_textinput)
        Clock.schedule_interval(self.labels.update, 20 / 60.0)
        # self.labels.create_label()
        self.labels.create_label()
        return self.labels


if __name__ == '__main__':
    MovingLabelApp().run()